#include <iostream>
using namespace std;



struct Node {
    char info;
    struct Node *l,*r;
    bool besokt;
    Node() {besokt = false; }
};

typedef Node * itemType;    //trenger ikke å bruke '*'(peker) ettersom den er typedef til itemtype
itemType z;


class Stack{
private:
    itemType *stack;
    int p;
public:
    Stack(int max=100)
    {stack = new itemType[max]; p = 0; }
    ~Stack() {delete stack;}
    inline void push(itemType v) {stack[p++] = v;}
    inline itemType pop() {return stack[--p];}
    inline int empty() {return !p;}
};

itemType oppretteTreet(){
    struct Node *x;
    char c;
    Stack stack(50);
    z = new Node; z->l = z; z->r = z; //setter 'z' til å peke på seg selv

    while((c = cin.get()) != '\n'){
        while(c == ' ')c = cin.get();
        x = new Node;
        x->info=c; x->l = z; x->r = z;
        if(c == '+' || c == '*'){
            x->r = stack.pop(); x->l = stack.pop();
        }
        stack.push(x);

    }
    return stack.pop();
}

void visit(itemType t)  {
 cout << t->info << ' ';  t->besokt = false;
}


//globale variabler
int antall = 0;
int antallEx = 0;
int antallFulle = 0;


//******* DEN JEG HAR LAGET*********//
int antallNoder(itemType t){
    if(t->l != z){antallNoder(t->l);}
    if(t->r != z){antallNoder(t->r);}

    return ++antall;
}

//********DEN FRODE HAR LAGET********//
/*
 * DENNE VIL VÆRE BEDRE FOR Å FINNE UT HVA SOM ER INTERNE NODER
 *
*/


int antNoder(itemType t)  {           //    totalt antall noder i hele treet.
  int antL = 0, antR = 0;
  if (t->l != z)  antL = antNoder(t->l);    //  Finner rekursivt antallet i
  if (t->r != z)  antR = antNoder(t->r);    //    venstre og h�yre subtre.
  return  1 + antL + antR;             //  Returnerer med subtreenes antall
}


//OPS DENNE BLIR FEIL! SE FRODE SIN LØSNINGSFORLSAG!
//EKSTERNE NODER = Z
int antallEksterne(itemType t){
    //int antallEx = 0;
    if(t->l != z)antallEksterne(t->l);
    if(t->r != z)antallEksterne(t->r);


    if(t->l == z && t->r == z)
        return ++antallEx;
    else
        return antallEx;
}

int antallFulleNoder(itemType t){
    if(t->l != z)antallFulleNoder(t->l);
    if(t->r != z)antallFulleNoder(t->r);

    if(t->l != z && t->r != z)
        ++antallFulle;
    return antallFulle;
}

int hoydeTreet(itemType t){
    int hoydeL = 0, hoydeR = 0;
    if(t->l != z) hoydeL = hoydeTreet(t->l);
    if(t->r != z) hoydeR = hoydeTreet(t->r);

    if(hoydeL>=hoydeR)
        return 1 + hoydeL;
    else
        return 1 + hoydeR;
}

int main(){


    //Bruker treet: 'A B + C D + +' for testing.
    itemType root = oppretteTreet();
    cout<<"Antall noder: "<<antallNoder(root)<<endl;
    cout<<"Antall eksterne: "<<antallEksterne(root)<<endl;
    cout<<"Antall fulle noder: "<<antallFulleNoder(root)<<endl;
    cout<<"Hoyde treet: "<<hoydeTreet(root)<<endl;

    return 0;
}































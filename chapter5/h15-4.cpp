//  Fil:  FRODEH \ ALG_MET \ EKSEMPEL \ EKS_06.CPP

//        Eksemplet viser bruk av rekursjon for permutering av en array.
//        Egen forklaring p� filen PERMUTERING.HTML.


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


const int N = 9;                       //  Arrayens lengde.

int arr[N];//= {9,1,5,7,4,2,6,3,8};                       //  Global array av int'er.
const int riktigSum = 100;


int gjorOmArrayTilInt(int fra, int til){//fra index til index
    int konversjon = 0;
    //Looper gjennom parameterne. Ganger med 10 for hver index plass
    //unntatt siste. array 1 2 3. ==== 1*10 + 2*10 + 3 = 123
    for(int i = fra; i<=til;i++){
        konversjon += arr[i];
        if(i<til)
            konversjon *= 10;
    }
    return konversjon;
}


    //Retunerer indexplassDeleTegn ved funn. Jobber rekursivt.
int plasseringAvDeleTegn(int indexplassDeleTegn, int indexPlassPluss){
    int venstreForPluss = 0, venstreForDele =0, hoyreForDele =0, hoyreForPluss = 0;
    int svar = 0;
    //Finner sum på venstre siden av plusstegnet:
    if(indexPlassPluss == 1)
        venstreForPluss = arr[0];
    else
        venstreForPluss = gjorOmArrayTilInt(0,1);

    //Viktig å holde tunga rett i munn her !!
    // 100 = xx + xxxx / xxx
    //summen til høyre tar tegnet til høyre for '/', mens tallet til venstre
    //tar det tallet der '/' blir plassert
    venstreForDele = gjorOmArrayTilInt(indexPlassPluss,indexplassDeleTegn);
    hoyreForDele = gjorOmArrayTilInt(indexplassDeleTegn+1,N-1);

    if(indexplassDeleTegn < N-3){
        plasseringAvDeleTegn(indexplassDeleTegn + 1,indexPlassPluss);
        hoyreForPluss = venstreForDele/hoyreForDele;
        svar = venstreForPluss + hoyreForPluss;//(venstreForDele/hoyreForDele);
        if(riktigSum == svar)
            cout << indexplassDeleTegn;
        else
            return -1;
    }
}

//funksjon kun for testing!!

void testFunksjon(){
    int indexPlassPluss =2,indexplassDeleTegn=5;

    int venstreForPluss = 0, venstreForDele =0, hoyreForDele =0;
    int svar = 0;
    //Finner sum på venstre siden av plusstegnet:
    if(indexPlassPluss == 1)
        venstreForPluss = arr[0];
    else
        venstreForPluss = gjorOmArrayTilInt(0,1);

    venstreForDele = gjorOmArrayTilInt(indexPlassPluss,indexplassDeleTegn);
    hoyreForDele = gjorOmArrayTilInt(indexplassDeleTegn+1,N-1);

    svar = venstreForPluss + (venstreForDele/hoyreForDele);
    if(riktigSum == svar)
        cout << indexplassDeleTegn<<endl;
    else
        cout <<"-1\n";

    for (int i = 0;  i < N;  i ++){
        cout << arr[i] << ' ';
    }
    cout<<"\n\nvenstreForPluss: "<<venstreForPluss;
    cout<<"\nvenstreForDele: "<<venstreForDele;
    cout<<"\nhoyreForDele: "<<hoyreForDele<<endl;
}

void finnRiktigeTall(int ar[]){
    int startIndexDele = 3;

    //sende med plassering av '+' kun to plasseringer!
    //Enten indexplass 1 eller 2
    // 1 + .../...  ||  12 + .../...
    int indexDeleFunn = plasseringAvDeleTegn(startIndexDele,1); // '+' start på index 1
    if(indexDeleFunn != -1){
        for (int i = 0;  i < N;  i ++){
            cout << ar[i] << ' ';
            if(i == 1)
                cout<<" + ";
            if(i == indexDeleFunn)
                cout << " / ";
        }
    }
    startIndexDele = 3;
    indexDeleFunn = plasseringAvDeleTegn(startIndexDele,2); // '+' start på index 2
    if(indexDeleFunn != -1){
        for (int i = 0;  i < N;  i ++){
            cout << ar[i] << ' ';
            if(i == 1)
                cout<<" + ";
            if(i == indexDeleFunn)
                cout << " / ";
        }
    }

}




void bytt(int & a1, int & a2)  {       //  Bytter om to array-elementer.
  int aa = a1;
      a1 = a2;
      a2 = aa;
}


void roterVenstre(int a[], int i)  {   //  Roterer elementer mot venstre.
  int x, k;
  x = a[i];                            //  Tar vare p� f�rste.
  for (k = i+1;  k < N;  k++)          //  Roterer:
    a[k-1] = a[k];
  a[N-1] = x;                          //  Legger f�rst inn bakerst.
}


void perm(int i)  {
  int t;
  if (i == N-1)                        //  N�dd en ny permutasjon:
     finnRiktigeTall(arr);                    //  Skriver ut arrayens innhold.
  else {                               //  Skal permutere:
     perm(i+1);                        //  Beholder n�v�rende nr.'i'.
                                       //    Permuterer resten.
     for (t = i+1; t < N; t++)  {
         bytt(arr[i], arr[t]);         //  Bytter nr.'i' etter tur med alle
                                       //    de andre etterf�lgende.
         perm(i+1);                    //  For hver av ombyttene: permuterer
     }                                 //    resten av de N� etterf�lgende.
     roterVenstre(arr, i);             //  Gjenoppretter opprinnelig array.
  }
}





int main()  {
  for (int i = 0;  i < N;  i++)        //  Fyller array med: 1, 2, 3, ..., N
      arr[i] = i+1;

  //testFunksjon();
  perm(0);                             //  Lager/skriver permutasjon av ALLE
                                       //    sifrene/arrayelementene.
  cout << '\n';
 finnRiktigeTall(arr);                       //  Skriver array etter at ferdig (for
  cout << "\n\n";                      //   � se om orginal er gjenopprettet).

  return 0;
}

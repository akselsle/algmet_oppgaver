#include <iostream>
#include <stdio.h>

using namespace std;


struct Node
  {  char info;  Node* l, * r;  bool besokt;
     Node()  { besokt = false; }   };


typedef Node* itemType;

itemType z;

class Stack  {               //  Fra side 26 i l‘reboka:
  private:
    itemType* stack;
    int p;
  public:
    Stack(int max = 100)
      {  stack = new itemType[max];  p = 0;  }
    ~Stack()
      {  delete [] stack;       }
    void push(itemType v)
      {  stack[p++] = v;     }
    itemType pop()
      {  return stack[--p];  }
    int empty()
      { return (!p);         }
};



itemType createTree()  {    //  Fra side 41 i l‘reboka:
  itemType x;
  char c;
  Stack stack(50);

  z = new Node;  z->l = z;  z->r = z;

  while ((c = cin.get()) != '\n')  {
    while (c == ' ')  cin.get(c);
    x = new Node();
    x->info = c;  x->l = z;  x->r = z;
    if (c == '+'  ||  c == '*')  {
       x->r = stack.pop();  x->l = stack.pop();
    }
    stack.push(x);
  }
  return stack.pop();
}

void visit(itemType t){
    cout<<t->info<<" ";
}


void traverseInorder(itemType t){
    if(t->l != z)traverseInorder(t->l);
    visit(t);
    if(t->r != z)traverseInorder(t->r);
}
void traversePostorder(itemType t){
    if(t->l != z)traverseInorder(t->l);
    if(t->r != z)traverseInorder(t->r);
    visit(t);
}
void traversePreorder(itemType t){
    visit(t);
    if(t->l != z)traverseInorder(t->l);
    if(t->r != z)traverseInorder(t->r);
}

int traversePostorderBytteOgRegne(itemType t){
    if(t->info == '+') return traversePostorderBytteOgRegne(t->l) + traversePostorderBytteOgRegne(t->r);
    else if(t->info == '*') return traversePostorderBytteOgRegne(t->l) * traversePostorderBytteOgRegne(t->r);
    else{
        cout << "\t" << t->info << ": ";
        int tall;    cin >> tall;   return tall;
    }

}

int main()  {
  //Bruker treet: 'A B + C D + +' for testing.
  itemType root;
  root = createTree();

  cout<<"Inorder: "; traverseInorder(root);
  cout<<"\nPostorder: "; traversePostorder(root);
  cout<<"\nPreorder: "; traversePreorder(root);
  cout<<"Regne sammen: "<<traversePostorderBytteOgRegne(root)<<endl;

  return 0;
}

#include <iostream>
#include <stdio.h> //strstr
#include <string.h>
using namespace std;


int N = 4;                       //  Arrayens lengde.
char arr[14];

                           //  Global array av int'er.
const char vokaler[] = "AEIOUYÆØÅ";
const int antallVokaler = 9;

int antallGodkjente = 0;
char antallFunnChar[1000+1];

bool erVokal(char c){
    for(int i=0;i<antallVokaler;i++)
        if(c == vokaler[i])
            return true;
    return false;
}

bool godkjent(char a[]){
    int vokalerEtterhverandre = 0;
    int konstEtterhverander = 0;
    if(a[0] != a[1]) //SJEKKER FOR e
    {
        for(int i=0;i<N;i++)
        {
            if(erVokal(a[i])) //vokalsjekk:
            {
                konstEtterhverander = 0;

                //Hvis i+1 går utenfor arrayen
                //om neste også er samme vokal
                //a:
                if(i+1<N && a[i]==a[i+1])
                    return false;
                //Hvis tre vokaler etter hverandre
                //b:
               if(++vokalerEtterhverandre == 3)
                   return false;

            }else   //konstonant sjekk:
            {
                vokalerEtterhverandre=0;

                //hvis i+2 ikke går utenfor arrayen
                //om tre like konst kommer etter hverandre:
                //c:
                if(i+2<N && a[i] == a[i+1] && a[i] == a[i+2])
                    return false;
                //Hvis fire konst etter hverandre:
                //d:
                if(++konstEtterhverander == 4)
                    return false;
            }

        }

    }else
        return false;


    return true;
}

void display(char a[])  {               //  Viser arrayens innhold.
  if(godkjent(a)){ 
    cout << "\n";
    cout<<"Nr."<<++antallGodkjente<<": \t";
    for (int i = 0;  i < N;  i ++){
      cout << a[i] << ' ';
      antallFunnChar[i] = a[i];
    }
  }
}


void bytt(char & a1, char & a2)  {       //  Bytter om to array-elementer.
  int aa = a1;
      a1 = a2;
      a2 = aa;
}


void roterVenstre(char a[], char i)  {   //  Roterer elementer mot venstre.
  int x, k;
  x = a[i];                            //  Tar vare p� f�rste.
  for (k = i+1;  k < N;  k++)          //  Roterer:
    a[k-1] = a[k];
  a[N-1] = x;                          //  Legger f�rst inn bakerst.
}


void perm(int i)  {
  int t;
  if (i == N-1)                        //  N�dd en ny permutasjon:
     display(arr);                     //  Skriver ut arrayens innhold.
  else {                               //  Skal permutere:
     perm(i+1);                        //  Beholder n�v�rende nr.'i'.
                                       //    Permuterer resten.
     for (t = i+1; t < N; t++)  {
         bytt(arr[i], arr[t]);         //  Bytter nr.'i' etter tur med alle
                                       //    de andre etterf�lgende.
         perm(i+1);                    //  For hver av ombyttene: permuterer
     }                                 //    resten av de N� etterf�lgende.
     roterVenstre(arr, i);             //  Gjenoppretter opprinnelig array.
  }
}


int main()  {
    //lag input senere
  char ordValgt[14];

   do{
    cout<<"Skriv inn ord: ";
    cin.getline(ordValgt,14);
    }while(strlen(ordValgt) >=14);

  N = strlen(ordValgt);

  for (int i = 0;  i < N;  i++)        //  Fyller array med: 1, 2, 3, ..., N
      arr[i] = ordValgt[i];

  perm(0);                             //  Lager/skriver permutasjon av ALLE
                                       //    sifrene/arrayelementene.
  cout << '\n';
  display(arr);                        //  Skriver array etter at ferdig (for
  cout << "\n\n";                      //   � se om orginal er gjenopprettet).

  cout<<"Totalt antall funn: "<<antallGodkjente<<endl;
  return 0;
}

#include <iostream>
using namespace std;

const int N = 4;
int vakkerDiagonalTeller = 0;
int brettet[N+1][N+1];

void plasser(int linje,int kolonne = 1);
bool godkjentPlassering(int kolonne, int linje);
bool kolonneSjekk(int kolonne, int linje);
bool diagonalSjekkVenstre(int kolonne);
bool diagonalSjekkHoyre(int linje,int kolonne);
void displayBrett();
int forrigeLinjeSinKolonne(int linje);
bool fulltBrettSjekk();
void nullstillBrettet();

int main(){
    nullstillBrettet();
    displayBrett();
    plasser(N);
    displayBrett();

    return 0;
}
void displayBrett(){
    for(int j=1;j<=N;j++){
        for(int i=1;i<=N;i++){
         cout<<brettet[i][j]<<" ";

        }
        cout<<"\n";
    }
    cout<<"\n\n";
}

int forrigeLinjeSinKolonne(int linje){
    for(int i=1;i<=N;i++){
        if(brettet[i][linje] == 1)
            return i;
    }
}
void nullstillBrettet(){
    //Nullstiller brettet:
    int j=1;
    for(int i=1;i<=N;i++){
        brettet[i][j] = 0;
        for(int j=1;j<=N;j++){
           brettet[i][j] = 0;
        }
    }
}
bool fulltBrettSjekk(){
    for(int i=1;i<=N;i++){
        if(brettet[i][N]==1)
            return true;
    }
    return false;
}

void plasser(int linje,int kolonne){
    if(linje==1){
        brettet[kolonne][linje] = 1;
        return;
        }
    plasser(linje-1);
    brettet[kolonne][linje] = 1;
    while(!godkjentPlassering(linje,kolonne)){
        if(kolonne<N){
            brettet[kolonne][linje]=0;
            ++kolonne;
            brettet[kolonne][linje] = 1;
            displayBrett();
        }
        else{
            int forrigeSinKolonne = forrigeLinjeSinKolonne(linje - 1 );
            brettet[linje-1][forrigeSinKolonne]=0;
            if(kolonne==N){
                brettet[kolonne][linje]=0;
                kolonne=1;
            }
            plasser(linje-1,forrigeSinKolonne+1);
        }
    }
    if(fulltBrettSjekk())
        displayBrett();


}


//sjekker om kollone medsendt linje ikke er tatt
bool kolonneSjekk(int linje, int kolonne){
    for(int i = 1; i<linje;i++){
        if(brettet[kolonne][i] == 1)
            return false;
    }
    return true;
}
//@author: _Simon&Aksel_
bool diagonalSjekkVenstre(int linje,int kolonne){
    bool godkjent = false;
        if(linje == 1 || kolonne == 1){
          if(vakkerDiagonalTeller != 0){
           if(brettet[kolonne][linje] == 1) //WHOPS THE DHOPS! FLIPP THA FLOPP
                return false;
            else
                return true;
          }else
               return true;
    }
    ++vakkerDiagonalTeller;
    godkjent = diagonalSjekkVenstre(linje-1,kolonne-1);
    --vakkerDiagonalTeller;
    //retunerer false hvis opptatt
    if(!godkjent)
        return false;
    if(vakkerDiagonalTeller != 0){
        if(brettet[kolonne][linje] == 1)
            return false;
        else
            return true;
    }else
        return true;
}



bool diagonalSjekkHoyre(int linje,int kolonne){
    bool godkjent = false;
        if(linje == 1 || kolonne == N){
          if(vakkerDiagonalTeller != 0){
           if(brettet[kolonne][linje] == 1) //WHOPS THE DHOPS! FLIPP THA FLOPP
                return false;
            else
                return true;
          }else
               return true;
    }
    ++vakkerDiagonalTeller;
    godkjent = diagonalSjekkHoyre(linje-1,kolonne+1);
    --vakkerDiagonalTeller;
    //retunerer false hvis opptatt
    if(!godkjent)
        return false;
    if(vakkerDiagonalTeller != 0){
        if(brettet[kolonne][linje] == 1)
            return false;
        else
            return true;
    }else
        return true;
}
bool godkjentPlassering(int linje, int kolonne){
        if(kolonneSjekk(linje,kolonne)){
            if(diagonalSjekkVenstre(linje,kolonne)){
                if(diagonalSjekkHoyre(linje,kolonne)){
                  return true;
            }else
                return false;
        }else
            return false;
    }else
        return false;
}

#include <iostream>
#include <thread>
using namespace std;

const int MAX = 8000;

int globaleLosninger[MAX*MAX];
int antallLosninger = 0;


bool unikLosning(int t);
void finnLosninger(int start, int slutt);
void skrivLosning();

int main(){
    int antallCalc = 8000/6;
    //hvordan compile: g++ -std=c++11 -pthread s18-4.cpp
    thread t1(finnLosninger,1,antallCalc);
    thread t2(finnLosninger,antallCalc,antallCalc*2);
    thread t3(finnLosninger,antallCalc*2,antallCalc*3);
    thread t4(finnLosninger,antallCalc*3,antallCalc*4);
    thread t5(finnLosninger,antallCalc*4,antallCalc*5);
    thread t6(finnLosninger,antallCalc*5,MAX);


    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t5.join();
    t6.join();

    cout<<"Antall losninger: "<< antallLosninger<<endl;

    return 0;
}

void finnLosninger(int start, int slutt){
    int forslag;
    for(int i = start; i<slutt+1;i++)
    {
        for(int j = i;j<slutt+1;j++)
        {
            forslag = i*j;
            if(unikLosning(forslag))
            {
                globaleLosninger[++antallLosninger] = forslag;
            }
        }
    }

}

void skrivLosning(){
    for(int i = 1;i<=antallLosninger;i++){
        cout << "Losning nr."<< i <<": \t"<<globaleLosninger[i]<<endl;
    }
}

bool unikLosning(int t){
    for(int i=1;i<=antallLosninger;i++){
        if(t == globaleLosninger[i])
        {
            return false;
        }
    }
    return true;
}

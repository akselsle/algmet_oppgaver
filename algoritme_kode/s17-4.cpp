#include <iostream>
#include <cstring>
using namespace std;

/*TANKE:
 * FOR ( 1 --> 1 000 000)
 * Gå gjennom hele tallet og splitte ved hver indeks:
 * Gjøre om til string
 * Sjekk indeks i+1 og stopp ved i-1
 * Gjøre om til int og regne ut:
 * Hvis
 *
 */


int main(){
    char buffer[20], strTall1[20],strTall2[20];
    int tall1,tall2,sum, tallLength, lteller = 0;

    for(int i=1;i<=1000000;i++){
        sprintf(buffer,"%d",i); //int to char-array
        tallLength = strlen(buffer); //finner lengden

        for(int j=0;j<tallLength-1;j++){
            strTall1[j] = buffer[j];

            for(int l = j+1;l < tallLength; l++){
                strTall2[lteller++] = buffer[l];
            }
            lteller = 0;

           tall1 = atoi(strTall1);
           tall2 = atoi(strTall2);
           if(((tall1+tall2)*(tall1+tall2)) == i)
               cout<<"("<<tall1 <<" + "<<tall2<<")^2 = "<<i<<endl;
           for(int m = 0;m<20;m++){ strTall2[m]='\0';}
        }

        for(int m = 0;m<20;m++){ strTall1[m]='\0';}
    }

    return 0;
}

#include <iostream>
using namespace std;


/*TANKE:
 * Trenger ikke å tenke på tallene: 0,1,8
 * Hvis tallene 6 || 9 er i en index at dere forekommer 9 || 6 på speilvendt index.
 *  DVS: Hvis arr[1] = 6 og lengden på tallet er 4, så må arr[4] = 9
 *       Arr => [6,8,8,9]
 *  Sjekke om tallene 2,3,4,5 eller 7. Hvis sant --> avbryt loop
 *
 * Konvertere fra int til int-array
 */



int snuOppNed(int n){
    int array[10], teller=0,number = n, backupTeller;
    while(number>0){
        number = number/10;
        ++teller;
    }
    backupTeller = teller;
    do{
      array[--teller] = n%10;
      n = n/10;
    }while(n > 0);

    for(int i=0;i<backupTeller;i++){
        //Sjekker om ulovlig tall:
        if(array[i] == 2 || array[i] == 3 || array[i] == 4
          || array[i] == 5 || array[i] == 7)
            return 0;
    }
    for(int i=0;i<backupTeller/10;i++){
        if(array[i] == 6)
            if(array[backupTeller-i] != 9)
                return 0;
        if(array[i] == 9)
            if(array[backupTeller-i] != 6)
                return 0;
        if(array[i] == 1)
            if(array[backupTeller-1] != 1)
                return 0;

        if(array[i] == 8)
            if(array[backupTeller-1] != 8)
                return 0;
        if(array[i] == 0)
            if(array[backupTeller-1] != 0)
                return 0;
    }
    return 1;
}

int main(){

    for(int i = 1;i<=100;i++){
       if(snuOppNed(i) != 0)
           cout<<i<<endl;
    }

    return 0;
}

/*
 *
 *
 *
 *
 *
 *  DENNE OPPGAVEN ER IKKE FULLFØRT!
 *  FØLES UT SOM EN COPY/PASTE OPPGAVE...
 *  LÆRTE IKKE MYE AV DENNE...
 *
 *
 *
 *
 *
 */
#include <iostream>
using namespace std;

struct Node {
    char info;
    struct Node *l,*r;
    bool besokt;
    Node() {besokt = false; }
};

typedef Node * itemType;

class Stack{
private:
    itemType *stack;
    int p;
public:
    Stack(int max=100)
    {stack = new itemType[max]; p = 0; }
    ~Stack() {delete stack;}
    inline void push(itemType v) {stack[p++] = v;}
    inline itemType pop() {return stack[--p];}
    inline int empty() {return !p;}
};

itemType oppretteTreet(){
    struct Node *x, *z;
    char c;
    Stack stack(50);
    z = new Node; z->l = z; z->r = z; //setter 'z' til å peke på seg selv

    while((c = cin.get()) != '\n'){
        while(c == ' ')c = cin.get();
        x = new Node;
        x->info=c; x->l = z; x->r = z;
        if(c == '+' || c == '*'){
            x->r = stack.pop(); x->l = stack.pop();
        }
        stack.push(x);

    }
    return stack.pop();
}

void visit(itemType t)  {
 cout << t->info << ' ';  t->besokt = false;
}

void preorder(itemType t){
    Stack stack(50);
    stack.push(t);
    while(!stack.empty()){
        t = stack.pop(); visit(t);
        if (t->r != z)  stack.push(t->r);
        if (t->l != z)  stack.push(t->l);
      }

}

int main(){
    itemType root = oppretteTreet();
    preorder(root);


    return 0;
}


















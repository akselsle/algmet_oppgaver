#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;



struct Node {
    char info;
    struct Node *l,*r;
    Node() { l=r=nullptr; }
};

typedef Node * itemType;    //trenger ikke å bruke '*'(peker) ettersom den er typedef til itemtype



class Stack{
private:
    itemType *stack;
    int p;
public:
    Stack(int max=100)
    {stack = new itemType[max]; p = 0; }
    ~Stack() {delete stack;}
    inline void push(itemType v) {stack[p++] = v;}
    inline itemType pop() {return stack[--p];}
    inline int empty() {return !p;}
};

itemType oppretteTreet(){
    struct Node *x;
    char c;
    Stack stack(50);
    while((c = cin.get()) != '\n'){
        while(c == ' ')c = cin.get();
        x = new Node;
        x->info=c;
        if(c == '+' || c == '*'){
            x->r = stack.pop(); x->l = stack.pop();
        }
        stack.push(x);

    }
    return stack.pop();
}

void visit(itemType t)  {
 cout << t->info << ' ';
}

 //************losning oppgave 3a)*********************
int venstreBlader = 0;
void venstreBladNoder(itemType n, bool erVenstre){
    if(n->l != nullptr)venstreBladNoder(n->l,1);
    if(n->l == nullptr && n->r == nullptr && erVenstre) ++venstreBlader; //Sjekker om det er venstre
    if(n->r != nullptr) venstreBladNoder(n->r,0);

}

//array med oversikt over når ting blir VISIT'a
char preOrderHoved[100];
char inOrderHoved[100];
char preOrderSub[100];
char inOrderSub[100];

//sammenligner sub-array med hovd-array og ser om sub finnes i hoved
//vet ikke hvor god løsning dette er.
bool finnesSubTreet(){
    if(strstr(preOrderHoved,preOrderSub))
    {
        if(strstr(inOrderHoved,inOrderSub))
            return true;
        else
            return false;
    }else
            return false;
    //EVENTUELT BRUK STRSTR PÅ HVER ARRAY OG SJEKKE OM DET FINNES ET FULL-SUB I HOVED TREET
}

int fyllArrayerPre(itemType n, char arr[],int i){
    arr[i++] = n->info;
    if(n->l != nullptr)i=fyllArrayerPre(n->l,arr,i);
    if(n->r != nullptr)i=fyllArrayerPre(n->r,arr,i);
    return i;
}
int fyllArrayerIn(itemType n, char arr[],int i){
    if(n->l != nullptr) i = fyllArrayerIn(n->l,arr,i);
    arr[i++] = n->info;
    if(n->r != nullptr) i = fyllArrayerIn(n->r,arr,i);
    return i;
    }
int main(){

    //Bruker treet root1: 'A B + C D + +' for testing.
    //Bruker treet root2: 'A B +' for testing
    itemType root1 = oppretteTreet();
    itemType root2 = oppretteTreet();
    venstreBladNoder(root1,0);
    cout<<"\tAntall venstre blader: "<<venstreBlader<<"\n";

    //OPPGAVE 3B:
    cout<<"\nRoot1(Hoved): \n";
    //Fyller hoved-arrayene:
    fyllArrayerIn(root1,inOrderHoved,0);
    cout<<"\n\n\tInOrderHoved: "<<inOrderHoved<<"\n";
    fyllArrayerPre(root1,preOrderHoved,0);
    cout<<"\n\tPreOrderHoved: "<<preOrderHoved<<"\n";

    cout<<"\nRoot2(Sub): \n";
    //Fyller sub-arrayene:
    fyllArrayerIn(root2,inOrderSub,0);
    cout<<"\n\tInOrderSub: "<<inOrderSub<<"\n";
    fyllArrayerPre(root2,preOrderSub,0);
    cout<<"\n\tPreOrderSub: "<<preOrderSub<<"\n";


    if(finnesSubTreet())
     cout<<"\n\n\t\tRoot2 er et subtre i Root1\n";
    else
        cout<<"\n\n\tRoot1 er IKKE et subtre i Root1\n";



    return 0;
}

#include <iostream>
using namespace std;
//*Bygger/oppretter/lager treet med constructor*//
struct Node {
    int ID;
    Node * left, * right;
    Node (int id, Node* l, Node* r)
         {ID = id; left = l, right = r;}
};
//FUNK:
void display(Node * n);
int finnMin(Node * n);
int finnMax(Node * n);
bool erBST(Node * n);


//GLOBALE:
Node * root;
//Node * rootTest = new Node(-1,nullptr,nullptr);
const int MIN = 0;
const int MAX = 9999;

int main(){
    Node * p[9];
    p[8] = new Node(8,nullptr,nullptr);
    p[7] = new Node(7,nullptr,p[8]);
    p[6] = new Node(6,nullptr,nullptr);
    p[5] = new Node(5,nullptr,nullptr);
    p[4] = new Node(4,nullptr,nullptr);
    p[3] = new Node(3,p[6],p[7]);
    p[2] = new Node(2,p[4],p[5]);
    root = new Node(1,p[2],p[3]);


    cout<<"Minste verdi: "<<finnMin(root)<<endl;
    cout<<"Største verdi: "<<finnMax(root)<<endl;
    (erBST(p[2])) ? cout<<"TREET ER BST" : cout<<"DET ER IKKE BST..";
    cout<<endl;


    return 0;
}
//      TREET:
//            1
//         /    \
//       2        3
//     /   \     /  \
//    4     5   6    7
//                    \
//                     8



int finnMax(Node * n){
    if (n == nullptr)  return MAX;
    int max = -1;
    if(n->left) max = finnMax(n->left);
    if(n->right) max = finnMax(n->right);
    //hvis verdi er større enn min, retuner ny verdi.
    return (n->ID > max) ? n->ID : max;
}

int finnMin(Node * n){
    if (n == nullptr)  return MIN;
    int min = MAX;
    if(n->left) min = finnMin(n->left);
    if(n->right) min = finnMin(n->right);
    //hvis verdi er mindre enn MAX, retuner ny verdi.
    return (n->ID < min) ? n->ID : min;
}

bool erBST(Node * n){
    bool godkjent = false;
    if(n->left) godkjent = erBST(n->left);
    if(n->right) godkjent = erBST(n->right);

    if(n->ID > finnMax(n->left) && n->ID <= finnMin(n->right))
        return true;
    else
        return false;
}

   //Preorder:
void display(Node * n){
    cout<<"Node: "<<n->ID<<endl;
    if(n->left) display(n->left);
    if(n->right) display(n->right);
}

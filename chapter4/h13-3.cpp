 #include <iostream>
using namespace std;

struct Node{
    int ID, height;
    Node * left, *right, *parent;
    Node(int id, Node *p)
    {ID = id; parent = p; height = 0; left = right = nullptr;}
};

Node * rot = nullptr;

void display(Node * n);
void insert(int id);
Node * distance(Node * p, int dist);
void longestPath(Node * p);

int main(){
    insert(17);
    insert(12);
    insert(28);
    insert(6);
    insert(11);
    insert(23);
    insert(34);
    //insert(35);
    //insert(36);
    display(rot);
    longestPath(rot);
    return 0;
}

void insert(int id){
    int hoydeOpp = 0;
    Node * n = rot, * p = nullptr;
    if(rot){

        //Finner hvor node skal være:
        while(n != nullptr) {
            p = n;
            n = (n->ID > id)? n->left : n->right;
        }
        //oppretter node og sender med id og parent.
        n = new Node(id,p);
        //Om ny node ble left eller right for parent.
        if(p->ID > id) p->left = n; else p->right = n;


        //oppdateres:
            while(n != nullptr){
                if(n->height <= hoydeOpp)
                     n->height = hoydeOpp++;
                n = n->parent;
            }


    }else
        rot = new Node(id,nullptr);
}

void display(Node * n){
    cout<<n->ID<<"\tHeight: "<<n->height<<endl;
    if(n->left != nullptr)display(n->left);
    if(n->right != nullptr)display(n->right);
}



    //FRODE SIN LØSNING:
Node * distance(Node * p, int dist){
    while (dist > 0)  {          //  Ikke kommet frem ennå:
      dist--;                    //  Teller ned til neste node.
                                 //  Har venstre, og lang nok stilengde:
      if (p->left != nullptr  &&  dist <= p->left->height)
         p = p->left;            //  Blar primært ned til venstre.
      else p = p->right;         //  Ellers MÅ det være ned til høyre,
    }                            //    pga. forutsetningen.
    return p;
}

void longestPath(Node * p){
    while(p->height > 0){
        if(p->left != nullptr && p->right->height >= p->left->height)
            p = p->right;
        else{
            if(p->left != nullptr)
                p = p->left;
            else
                p = p->right;
        }
    }
    cout<<"Node ID: "<<p->ID;
}




















#include <iostream>
using namespace std;

struct Node {
    int ID;
    Node *left, *right;
    Node (int id){ID=id;left=right=nullptr;}
};

//Globale:
Node * root = nullptr;
int hoyde = 0; //nivå + 1

//funk:
void insert(int id);
void display(Node * n);
Node * mindreEnn(int v);

int main(){
    insert(81);
    insert(80);
    insert(90);
    insert(87);
    insert(94);
    insert(86);
    display(root);
    Node * x = mindreEnn(88);
    if(x == nullptr)
        cout<<"NULLPTR"<<endl;
    else
        cout<<"NODE SIN VERDI : "<<x->ID<<endl;

    return 0;
}

Node * mindreEnn(int v){
    Node * x = root;
    Node * forrigeNode = root;
    if(x == nullptr)
        return nullptr;

    while(x)
    {

        //Hvis de er like OG venstre finnes
        //Da er nærmeste mindre den til venstre for 'x'.
        //ELSE: ER DET DEN FØR 'x':
        if(x->ID == v)
        {
            if(x->left != nullptr)
            {
                 return x->left;
            }
            else
            {
               if(forrigeNode->left != x)
                  return forrigeNode;
               else
                   return nullptr;
            }
        }

        if(x->left == nullptr && x->right == nullptr)
            return x;

        //Hvis 'x' er mindre enn v OG
        //den til høyre er større enn v.
        //Da skal 'x' retuneres
        if(x->ID < v && x->right->ID > v)
        {
            if(x->right->left != nullptr)
                return x->right->left;
            else
                return x;
        }


        forrigeNode = x;
        x = (v < x->ID) ? x->left : x->right;
    }
    return nullptr;
}

void insert(int id){
    int lokalHoyde = 1;
    if(root){
        Node * x = root, * enBak = root;
        while(x)
        {
            //Finner frem til riktig posisjon, men 'enBak' er en bak.
            enBak = x;
            x = (id < x->ID) ? x->left : x->right;
            ++lokalHoyde;
            if(lokalHoyde>hoyde)
                ++hoyde;

        }
        x = new Node(id);
        if(id < enBak->ID) enBak->left = x; else enBak->right = x;

    }else{
        root = new Node(id);
        ++hoyde;
    }
}
void display(Node * n){
    cout<<"Node: \t"<<n->ID<<endl;
    if(n->left) display(n->left);
    if(n->right) display(n->right);
}

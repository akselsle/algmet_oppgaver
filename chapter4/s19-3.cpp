#include <iostream>
using namespace std;


struct Node  {
  int ID;                      //  Nodens ID/key/nøkkel/navn (et tall).
  Node *left, *right;          //  Peker til begge subtrærne (evt. 'nullptr').
  Node(int id)  {  ID = id;  left = right = nullptr;  }
};
                    // 2x rot-pekere (har altså ikke at  head->right er rota):
Node* root1 = nullptr, *root2 = nullptr;
void byggTre(int n);
bool erToTrerIdentiske(Node * t1, Node * t2);
int traverse(Node * t);

int main() {
    byggTre(1);     byggTre(2);
    (erToTrerIdentiske(root1,root2))?cout<<"LIKE":cout<<"IKKE LIKE";
    return 0;
}

void byggTre(int n) {
    Node* p[12];
    for (int i = 1; i <= 11; i++)  p[i] = new Node(i);
    if (n == 1) root1 = p[1];  else  root2 = p[1];   //  Bygger treet:     //
    p[1]->left = p[2];    p[1]->right = p[3];  //            1             //
    p[2]->left = p[4];    p[2]->right = p[5];  //        /       \         //
    p[3]->left = p[6];    p[3]->right = p[7];  //       2         3        //
    p[5]->left = p[8];                         //     /   \     /   \      //
                          p[6]->right = p[9];  //    4     5   6     7     //
    p[7]->left = p[10];   p[7]->right = p[11]; //         /     \   / \    //
}                                              //        8       9 10 11   //

int traverse(Node * t){
    if(t->left != nullptr)traverse(t->left);
    cout<< t->ID<<" ";
    if(t->right != nullptr) traverse(t->right);

    return 1;
}

bool erToTrerIdentiske(Node * t1, Node * t2){


    if(traverse(t1) == traverse(t2))
        return true;
    else
        return false;
}

#include <iostream>
using namespace std;


/*
 * Notat:
 *       Min løsning blir ikke komplett
 *       ettersom den ikke klarer å sjekke
 *       en over seg hvis partall ikke
 *       er på høyeste nivå.
 */

struct Node  {
  int ID;                      //  Nodens ID/key/nøkkel/navn (et tall).
  Node *left, *right;          //  Peker til begge subtrærne (evt. 'nullptr').
  Node(int id)  {  ID = id;  left = right = nullptr;  }
};
                    // 2x rot-pekere (har altså ikke at  head->right er rota):
Node* root = nullptr;

int maxHOT = 0, y = 0;

void byggTre(int n);
int finneHoyde(Node * n);
void traverse3A(Node* t);
void visit3A(Node* t);

int main() {
    byggTre(1);
    int hoyde = finneHoyde(root);
    if(hoyde == 1 || hoyde == 2){
        cout<<"MAX NIVÅ: 0\n";
    }else if(hoyde %  2 != 0)
    {
        cout<<"Partall MAKS NIVÅ: "<<hoyde<<endl;
    }else
    {
    }

    //*****FRODE: ******
    //  Tester 3A:
    traverse3A(root);
    cout << "\nHøyeste nivå for oddetalls terminalnode er: " << maxHOT << '\n';

    return 0;
}


int finneHoyde(Node * n){
    int hoydeL = 0, hoydeR = 0;
    if(n->left != nullptr) hoydeL = finneHoyde(n->left);
    if(n->right != nullptr) hoydeR = finneHoyde(n->right);
    return (hoydeL>=hoydeR) ? 1 + hoydeL : 1 + hoydeR;
}

void byggTre(int n) {
    Node* p[13];
    for (int i = 1; i <= 12; i++)  p[i] = new Node(i);
    if (n == 1) root = p[1];                      //  Bygger treet:     //
    p[1]->left = p[2];    p[1]->right = p[3];  //            1             //
    p[2]->left = p[4];    p[2]->right = p[5];  //        /       \         //
    p[3]->left = p[6];    p[3]->right = p[7];  //       2         3        //
    p[5]->left = p[8];                         //     /   \     /   \      //
                          p[6]->right = p[9];  //    4     5   6     7     //
    p[7]->left = p[10];   p[7]->right = p[11]; //         /     \   / \    //
    p[10]->left = p[12];                       //        8       9 10 11   //
                                              //                  /        //
                                              //                12        //
}



//*********FRODE SIN LØSNING******************//

void visit3A(Node* t) {      //  Terminalnode, på oddetalls nivå og ny rekord:
    if (!t->left && !t->right && (y % 2) && (y > maxHOT))
        maxHOT = y;
}

void traverse3A(Node* t) {         //  Avskrift av s.61 i læreboka.
    y++;
    if (t) {
       visit3A(t);                 //  Vilkårlig om treet visiteres
       traverse3A(t->left);        //  pre-, in- eller post-order.
       traverse3A(t->right);       //  Dvs. 'visit3A' kan stå før, mellom
    }                              //  eller etter de rekursive kallene.
    y--;
}



#include <iostream>
using namespace std;

const double MILLION = 100000000;

double finnTotalsum();

int main(){
    double sum = 0;
    for(int i = 1;i<=MILLION;i++){
        if(i % 7 == 0 && i % 5 != 0)
        {
            sum += i;
        }
    }
    cout<<"Total Sum: "<<sum<<endl;
    cout<<"Frod Sum: "<<finnTotalsum()<<endl;
    return 0;
}


//*************FRODE SIN LOSNING!!!************
double finnTotalsum()  {      //  ('float' er også helt greit nok,
   double sum = 0.0F;         //    selv om svaret ikke blir helt riktig.)
                              //  (Start på '0' er også greit:)
   for (int i = 7;  i <= 100000000;  i += 7)  //  Øk med syv og syv:
       if (i % 5)  sum += i;  //  IKKE heltallelig delelig: øk totalsummen.
   return sum;                //  Returnerer totalsummen.
}

#include <iostream>
#include <stdlib.h>
using namespace std;

const int ANTDAGER = 30;
int kurser[ANTDAGER+1];
int main(){
    int valgteMax[3], valgteMin[3]; //to verdier for min max. //Max[1] har relasjon Min[1] i form av verdi
                                    //Max[2] og Min[2] ligger den faktiske dagen.
    int ikkeGodkjentMinDager[ANTDAGER/2];
    int antallIkkeGodkjenteMinDager=0;
    bool godkjentDag=true;
    srand(time(NULL));
    //setter random verdier:
    for(int i=1;i<=ANTDAGER;i++)
    {
        kurser[i] = rand() % 100 + 1;
    }

    valgteMin[1]=kurser[1];valgteMin[2]=1;
    valgteMax[1]=kurser[2];valgteMax[2]=2;

    while(true)
    {
      for(int i=1;i<=ANTDAGER;i++)
      {
        if(kurser[i]<valgteMin[1])
        { //den er mindre:
            for(int j=1;j<=antallIkkeGodkjenteMinDager;j++)
            {
                if(ikkeGodkjentMinDager[j] == i)
                    godkjentDag = false;
            }

                if(godkjentDag)
                {
                 valgteMin[1]=kurser[i];//verdi
                 valgteMin[2]=i;        //dag
                }
        }
        if(kurser[i]>valgteMax[1])
           { //den er større:
               valgteMax[1]=kurser[i];//verdi
               valgteMax[2]=i;        //dag
           }
      }

      if(valgteMin[2]<valgteMax[2])
          break;
      else
      {
        ikkeGodkjentMinDager[++antallIkkeGodkjenteMinDager] = valgteMin[2];
        valgteMin[1]=kurser[1];valgteMin[2]=1;
        valgteMax[1]=kurser[2];valgteMax[2]=2;

      }

    }


    //displayer:
    for(int i=1;i<=ANTDAGER;i++) cout<<kurser[i]<<" ";

    cout<<"\n\n";

    cout<<"Det ville lonn seg å kjope: \n"
        <<"\tKJOP VERDI: "<<valgteMin[1]<<"\tDag: "<<valgteMin[2]<<"\n"
        <<"\tSOLGT VERDI: "<<valgteMax[1]<<"\tDag: "<<valgteMax[2]<<"\n";


    return 0;
}
